package org.example.second_task; // Объявление пакета для организации кода.

import java.util.ArrayList;
import java.util.List;

/**
 * В данном классе будет содержаться inner класс с входными параметрами, а также решение задачи
 * коммивояжера по разомкнутому контуру. Контур разомкнут, поскольку мы решаем прикладную задачу
 * сверлим отверстия. Так как мы просверлили отверстия в начальной точке, то возвращаться нам в нее
 * не нужно, чтобы не сверлить там дважды.
 */
public class TravellingSalesmanProblem {
    // Объявление и определение класса с именем TravellingSalesmanProblem.

    public TravellingSalesmanProblem() {
    }
    // Конструктор класса TravellingSalesmanProblem.

    static class DataModel {

        public final int[][] locations = {{288, 149}, {288, 129}, {270, 133}, {256,
                141}, {256, 157},
                {246, 157}, {236, 169}, {228, 169}, {228, 161}, {220, 169}, {212, 169},
                {204, 169},
                {196, 169}, {188, 169}, {196, 161}, {188, 145}, {172, 145}, {164, 145},
                {156, 145},
                {148, 145}, {140, 145}, {148, 169}, {164, 169}, {172, 169}, {156, 169},
                {140, 169},
                {132, 169}, {124, 169}, {116, 161}, {104, 153}, {104, 161}, {104, 169},
                {90, 165},
                {80, 157}, {64, 157}, {64, 165}, {56, 169}, {56, 161}, {56, 153}, {56,
                145}, {56, 137},
                {56, 129}, {56, 121}, {40, 121}, {40, 129}, {40, 137}, {40, 145}, {40,
                153}, {40, 161},
                {40, 169}, {32, 169}, {32, 161}, {32, 153}, {32, 145}, {32, 137}, {32,
                129}, {32, 121},
                {32, 113}, {40, 113}, {56, 113}, {56, 105}, {48, 99}, {40, 99}, {32, 97},
                {32, 89},
                {24, 89}, {16, 97}, {16, 109}, {8, 109}, {8, 97}, {8, 89}, {8, 81}, {8,
                73}, {8, 65},
                {8, 57}, {16, 57}, {8, 49}, {8, 41}, {24, 45}, {32, 41}, {32, 49}, {32,
                57}, {32, 65},
                {32, 73}, {32, 81}, {40, 83}, {40, 73}, {40, 63}, {40, 51}, {44, 43}, {44,
                35}, {44, 27},
                {32, 25}, {24, 25}, {16, 25}, {16, 17}, {24, 17}, {32, 17}, {44, 11}, {56,
                9}, {56, 17},
                {56, 25}, {56, 33}, {56, 41}, {64, 41}, {72, 41}, {72, 49}, {56, 49}, {48,
                51}, {56, 57},
                {56, 65}, {48, 63}, {48, 73}, {56, 73}, {56, 81}, {48, 83}, {56, 89}, {56,
                97}, {104, 97},
                {104, 105}, {104, 113}, {104, 121}, {104, 129}, {104, 137}, {104, 145},
                {116, 145},
                {124, 145}, {132, 145}, {132, 137}, {140, 137}, {148, 137}, {156, 137},
                {164, 137},
                {172, 125}, {172, 117}, {172, 109}, {172, 101}, {172, 93}, {172, 85},
                {180, 85}, {180, 77},
                {180, 69}, {180, 61}, {180, 53}, {172, 53}, {172, 61}, {172, 69}, {172,
                77}, {164, 81},
                {148, 85}, {124, 85}, {124, 93}, {124, 109}, {124, 125}, {124, 117}, {124,
                101}, {104, 89},
                {104, 81}, {104, 73}, {104, 65}, {104, 49}, {104, 41}, {104, 33}, {104,
                25}, {104, 17},
                {92, 9}, {80, 9}, {72, 9}, {64, 21}, {72, 25}, {80, 25}, {80, 25}, {80,
                41}, {88, 49},
                {104, 57}, {124, 69}, {124, 77}, {132, 81}, {140, 65}, {132, 61}, {124,
                61}, {124, 53},
                {124, 45}, {124, 37}, {124, 29}, {132, 21}, {124, 21}, {120, 9}, {128, 9},
                {136, 9},
                {148, 9}, {162, 9}, {156, 25}, {172, 21}, {180, 21}, {180, 29}, {172, 29},
                {172, 37},
                {172, 45}, {180, 45}, {180, 37}, {188, 41}, {196, 49}, {204, 57}, {212,
                65}, {220, 73},
                {228, 69}, {228, 77}, {236, 77}, {236, 69}, {236, 61}, {228, 61}, {228,
                53}, {236, 53},
                {236, 45}, {228, 45}, {228, 37}, {236, 37}, {236, 29}, {228, 29}, {228,
                21}, {236, 21},
                {252, 21}, {260, 29}, {260, 37}, {260, 45}, {260, 53}, {260, 61}, {260,
                69}, {260, 77},
                {276, 77}, {276, 69}, {276, 61}, {276, 53}, {284, 53}, {284, 61}, {284,
                69}, {284, 77},
                {284, 85}, {284, 93}, {284, 101}, {288, 109}, {280, 109}, {276, 101},
                {276, 93}, {276, 85},
                {268, 97}, {260, 109}, {252, 101}, {260, 93}, {260, 85}, {236, 85}, {228,
                85}, {228, 93},
                {236, 93}, {236, 101}, {228, 101}, {228, 109}, {228, 117}, {228, 125},
                {220, 125},
                {212, 117}, {204, 109}, {196, 101}, {188, 93}, {180, 93}, {180, 101},
                {180, 109},
                {180, 117}, {180, 125}, {196, 145}, {204, 145}, {212, 145}, {220, 145},
                {228, 145},
                {236, 145}, {246, 141}, {252, 125}, {260, 129}, {280, 133}};
        public final int vehicleNumber = 1;
        public final int depot = 0;
    }

    private List<int[]> resultList = new ArrayList<>();
    // Объявление приватного поля resultList для хранения результата.

    public List<int[]> getResultList() {
        // Объявление метода getResultList для получения результата.

        return this.resultList;
        // Возвращение результата (списка посещенных вершин).
    }

    private List<Integer> resultBeforeEnhance = new ArrayList<>();
    // Объявление приватного поля resultBeforeEnhance для хранения результата до улучшения.

    public List<Integer> getResultBeforeEnhance() {
        // Объявление метода getResultBeforeEnhance для получения результата.

        return this.resultBeforeEnhance;
        // Возвращение результата (списка посещенных вершин).
    }

    private List<Integer> resultListIndex = new ArrayList<>();
    // Объявление приватного поля resultListIndex для хранения индексов точек в порядке посещения.

    public List<Integer> getResultListIndex() {
        // Объявление метода getResultListIndex для получения результата.

        return this.resultListIndex;
        // Возвращение результата (индексы списка посещенных вершин).
    }

    private static double distance(int[] firstVertex, int[] secondVertex) {
        // Объявление статического метода для вычисления расстояния между двумя вершинами.

        int x1 = firstVertex[0]; // Извлечение координаты х первой вершины.
        int y1 = firstVertex[1]; // Извлечение координаты у первой вершины.
        int x2 = secondVertex[0]; // Извлечение координаты х второй вершины.
        int y2 = secondVertex[1]; // Извлечение координаты у второй вершины.

        return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
        // Вычисление и возвращение расстояния между вершинами с использованием формулы Евклидова расстояния.
    }

    private static void reverse(List<int[]> list, int start, int end) {
        // Объявление статического метода для разворачивания части списка от start до end.

        while (start < end) { // разворачиваем, пока индекс начала меньше индекса конца
            int[] temp = list.get(start); // сохраняем в промежуточный результат точку начала
            list.set(start, list.get(end)); // ставим по индексу start значение индекса end
            list.set(end,
                    temp); // по индексу end ставим значение temp элемента (в котором находится значение индекса start)
            start++; // инкремент start
            end--; // декремент end
        }
    }

    public void taskSolution() {
        // Объявление метода для решения задачи TSP.

        DataModel dataModel = new DataModel();
        // Создание экземпляра класса DataModel для хранения данных.

        try {
            // Обработка возможных исключений.

            double length = 0; // Инициализация переменной для хранения длины маршрута.

            List<int[]> visitedVertex = new ArrayList<>(); // Создание списка для хранения посещенных вершин.

            visitedVertex.add(
                    dataModel.locations[0]); // Добавление первой вершины в список посещенных.

            int currentIndex = 0; // Инициализация индекса текущей вершины
            int nextIndex = 0; // Инициализация индекса следующей вершины.

            while (visitedVertex.size() < dataModel.locations.length) {
                // Пока не посетили все вершины.

                double currentLength = Double.MAX_VALUE; // Инициализация текущей длины максимальным значением.

                for (int i = 0; i < dataModel.locations.length; ++i) {
                    // Перебор всех вершин.

                    if (!visitedVertex.contains(dataModel.locations[i])) {
                        // Если вершина не посещена.

                        double distance = Math.sqrt(Math.pow(
                                (dataModel.locations[currentIndex][0] - dataModel.locations[i][0]),
                                2) +
                                Math.pow((dataModel.locations[currentIndex][1]
                                        - dataModel.locations[i][1]), 2));
                        // Вычисление расстояния между текущей и другой вершиной.

                        if (distance
                                < currentLength) { // если вычисленное расстояние меньше текущего минимального
                            currentLength = distance; //текущее минимальное расстояние = вычисленному расстоянию
                            nextIndex = i; //говорим, что в следующий раз будем смотреть расстояние от той вершины,
                            // до которой нам сейчас ближе всего
                        }
                    }
                }
                length += currentLength;
                visitedVertex.add(dataModel.locations[nextIndex]);
                currentIndex = nextIndex;
                // После прохождения всех вершин делаем добавление следующей вершины в список и обновление текущего индекса.
                // к общей длине пути прибавляем расстояние до ближайшей не посещенной вершины
            }

            System.out.println("Result by using nearest neighbors algorithm: " + length);
            //вывод результата алгоритма поиска ближайшего соседа

            for (int[] elem : visitedVertex) {
                for (int i = 0; i < dataModel.locations.length; ++i) {
                    if (dataModel.locations[i] == elem) {
                        this.resultBeforeEnhance.add(i);
                    }
                }
            }

            System.out.println("Route before 2-opt:");
            for (int i = 0; i < visitedVertex.size() - 1; ++i) {
                System.out.print(
                        "{" + visitedVertex.get(i)[0] + ", " + visitedVertex.get(i)[1] + "} -> ");
            }
            System.out.println(
                    "{" + visitedVertex.get(visitedVertex.size() - 1)[0] + ", " + visitedVertex.get(
                            visitedVertex.size() - 1)[1] + "}");

            System.out.println("Start using 2-opt algorithm");
            boolean improved = true; // заводим переменную чтобы понять, когда результат уже не будет улучшен

            while (improved) {
                // Пока улучшения возможны.

                improved = false;

                for (int i = 1; i < visitedVertex.size() - 1; i++) {
                    for (int j = i + 1; j < visitedVertex.size() - 1; j++) {
                        double delta = -distance(visitedVertex.get(i), visitedVertex.get(i - 1))
                                - distance(visitedVertex.get(j), visitedVertex.get(j + 1))
                                + distance(visitedVertex.get(i - 1), visitedVertex.get(j))
                                + distance(visitedVertex.get(i), visitedVertex.get(j + 1));
                        //берем расстояние между вершинами i, i+1 и j, j+1 , и смотрим изменится ли оно
                        //если мы поменяем вершины местами: соединим вершины i-1, j и i, j+1

                        if (delta < 0) {
                            // Если улучшение найдено.

                            reverse(visitedVertex, i, j);
                            // разворачиваем часть списка от индекса i до j.

                            improved = true; // улучшили результат, ставим флаг
                        }
                    }
                }
            }
            double lengthOptimized = 0; // заводим переменную, в которой будет храниться оптимизированная длина

            for (int i = 0; i < visitedVertex.size() - 1; i++) {
                lengthOptimized += distance(visitedVertex.get(i), visitedVertex.get(i + 1));
            }
            // После завершения алгоритма 2-opt вычисляется оптимизированная длина маршрута.

            System.out.println("Length after 2-opt optimization: " + lengthOptimized);
            this.resultList = visitedVertex;

            System.out.println("Route after 2-opt:");
            for (int i = 0; i < visitedVertex.size() - 1; ++i) {
                System.out.print(
                        "{" + visitedVertex.get(i)[0] + ", " + visitedVertex.get(i)[1] + "} -> ");
            }
            System.out.println(
                    "{" + visitedVertex.get(visitedVertex.size() - 1)[0] + ", " + visitedVertex.get(
                            visitedVertex.size() - 1)[1] + "}");

            for (int[] elem : visitedVertex) {
                for (int i = 0; i < dataModel.locations.length; ++i) {
                    if (dataModel.locations[i] == elem) {
                        this.resultListIndex.add(i);
                    }
                }
            }

            assert visitedVertex.size() == dataModel.locations.length;
            assert this.resultListIndex.size() == visitedVertex.size();
            // Проверка размера списка посещенных вершин.

        } catch (Exception exception) {
            System.out.println("Error during solving the TSP: " + exception.getMessage());
            // Обработка и вывод ошибки, если она возникла.
        }
    }
}
