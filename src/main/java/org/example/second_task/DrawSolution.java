package org.example.second_task;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.util.List;
/**
 * Визуализатор маршрута для задачи коммивояжёра.
 */
public class DrawSolution extends Application {
    private static int[][] LOCATIONS;
    private static List<Integer> ROUTE;
    private static final int CANVAS_WIDTH = 1280;
    private static final int CANVAS_HEIGHT = 720;
    private static final int PADDING = 50; // отступ для осей
    /**
     * Начальный метод для создания графического интерфейса.
     *
     * @param primaryStage Основное окно приложения.
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Travelling Salesman Route");
        Canvas canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        drawRoute(canvas.getGraphicsContext2D());
        Pane pane = new Pane();
        pane.getChildren().add(canvas);
        primaryStage.setScene(new Scene(pane));
        primaryStage.show();
    }
    /**
     * Отрисовывает маршрут на заданном графическом контексте.
     *
     * @param gc Графический контекст для отрисовки.
     */
    private void drawRoute(GraphicsContext gc) {
        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE,
                maxX = 0, maxY = 0;
        for (int[] location : LOCATIONS) {
            if (location[0] < minX) minX = location[0];
            if (location[0] > maxX) maxX = location[0];
            if (location[1] < minY) minY = location[1];
            if (location[1] > maxY) maxY = location[1];
        }
        int roundedMaxX = (int) Math.ceil((double) maxX / 10) *
                10;
        int roundedMaxY = (int) Math.ceil((double) maxY / 10) *
                10;
        double scaleX = (CANVAS_WIDTH - 2.0 * PADDING) /
                (roundedMaxX);
        double scaleY = (CANVAS_HEIGHT - 2.0 * PADDING) /
                (roundedMaxY);
        // Отрисовка осей
        gc.setStroke(Color.GRAY);
        gc.strokeLine(PADDING, CANVAS_HEIGHT - PADDING,
                CANVAS_WIDTH - PADDING, CANVAS_HEIGHT - PADDING); // ось X
        gc.strokeLine(PADDING, PADDING, PADDING, CANVAS_HEIGHT -
                PADDING); // ось Y
        // Нанесение разметки на оси
        for (int i = 0; i <= roundedMaxX; i += 10) {
            double x = PADDING + i * scaleX;
            gc.fillText(String.valueOf(i), x, CANVAS_HEIGHT -
                    PADDING + 20);
        }
        for (int i = 0; i <= roundedMaxY; i += 10) {
            double y = CANVAS_HEIGHT - PADDING - i * scaleY;
            gc.fillText(String.valueOf(i), PADDING - 30, y);
        }
        gc.setFill(Color.BLUE);
        for (int[] location : LOCATIONS) {
            double x = PADDING + location[0] * scaleX;
            double y = CANVAS_HEIGHT - PADDING - location[1] *
                    scaleY;
            gc.fillOval(x, y, 5, 5);
        }
        gc.setStroke(Color.RED);
        for (int i = 0; i < ROUTE.size() - 1; i++) {
            double x1 = PADDING + LOCATIONS[ROUTE.get(i)][0] *
                    scaleX;
            double y1 = CANVAS_HEIGHT - PADDING -
                    LOCATIONS[ROUTE.get(i)][1] * scaleY;
            double x2 = PADDING + LOCATIONS[ROUTE.get(i + 1)][0]
                    * scaleX;
            double y2 = CANVAS_HEIGHT - PADDING -
                    LOCATIONS[ROUTE.get(i + 1)][1] * scaleY;
            gc.strokeLine(x1, y1, x2, y2);
        }
    }
    /**
     * Запускает визуализатор для заданного маршрута и локаций.
     *
     * @param locations Координаты точек.
     * @param route Маршрут коммивояжёра.
     */
    public static void visualizeRoute(int[][] locations,
            List<Integer> route) {
        DrawSolution.LOCATIONS = locations;
        DrawSolution.ROUTE = route;
        Application.launch(DrawSolution.class);
    }
}
