package org.example.first_task; // Указываем пакет, в котором находится наш класс

import java.util.List; // Импортируем интерфейс List из пакета java.util для работы со списками

/**
 * В данном классе реализован бинарный поиск элемента в массиве. Идея: Берём средний элемент из
 * массива: если он совпадает с искомым, задача решена. Если искомое значение больше, оно должно
 * быть в правой части массива. Если меньше — в левой. Продолжаем поиск в соответствующей части.
 */
public class BinarySearch { // Объявляем класс BinarySearch

    /**
     * Бинарный поиск элемента в отсортированном списке.
     *
     * @param element   элемент, который нужно найти
     * @param inputList список, в котором производится поиск
     * @return индекс найденного элемента или -1, если элемент отсутствует
     */
    public int findElement(Integer element,
            List<Integer> inputList) { // Объявляем метод findElement с двумя параметрами: искомым элементом и списком для поиска

        long startTime = System.nanoTime(); // Запоминаем время начала выполнения функции

        if (inputList == null || inputList.isEmpty()) { // Проверяем, не пуст ли список
            System.out.println(
                    "Error! List is empty! Result = -1."); // Выводим сообщение об ошибке, если список пуст
            long endTime = System.nanoTime(); // Запоминаем время завершения выполнения функции
            long duration = endTime - startTime; // Вычисляем время выполнения функции
            System.out.println("Element not found! Time taken: " + duration
                    + " nanoseconds"); // Выводим время выполнения
            return -1; // Возвращаем -1, поскольку элемент не может быть найден в пустом списке
        }

        try { // Начинаем блок try для обработки возможных исключений
            int left_boundary = 0; // Устанавливаем левую границу поиска в начало списка
            int right_boundary =
                    inputList.size() - 1; // Устанавливаем правую границу поиска в конец списка

            System.out.println(
                    "Start finding process"); // Выводим сообщение о начале процесса поиска
            while (left_boundary <= right_boundary) { // Пока левая граница не пересеклась с правой
                int mid = left_boundary + (right_boundary - left_boundary)
                        / 2; // Находим средний индекс в текущем диапазоне

                if (inputList.get(mid)
                        .equals(element)) { // Если элемент в середине диапазона равен искомому
                    long endTime = System.nanoTime(); // Запоминаем время завершения выполнения функции
                    long duration = endTime - startTime; // Вычисляем время выполнения функции
                    System.out.println("Element found! Time taken: " + duration
                            + " nanoseconds"); // Выводим время выполнения
                    return mid; // Возвращаем индекс найденного элемента
                }

                if (element > inputList.get(
                        mid)) { // Если искомый элемент больше элемента в середине диапазона
                    left_boundary = mid + 1; // Сужаем диапазон поиска, смещая левую границу
                } else { // Если искомый элемент меньше элемента в середине диапазона
                    right_boundary = mid - 1; // Сужаем диапазон поиска, смещая правую границу
                }
            }

            long endTime = System.nanoTime(); // Запоминаем время завершения выполнения функции
            long duration = endTime - startTime; // Вычисляем время выполнения функции
            System.out.println("Element not found! Time taken: " + duration
                    + " nanoseconds"); // Выводим время выполнения

            return -1; // Если элемент не найден, возвращаем -1
        } catch (
                Exception exception) { // Ловим любые исключения, которые могли произойти в блоке try
            System.out.println("Error during binary searching: "
                    + exception.getMessage()); // Выводим сообщение об ошибке с описанием исключения
            return -1; // Возвращаем -1, так как поиск завершился неудачно
        }
    }
}