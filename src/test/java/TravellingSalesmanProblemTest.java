import org.example.second_task.TravellingSalesmanProblem; // Импорт класса tsp для проведения тестов
import org.junit.jupiter.api.Test; // Импорт аннотации @Test для JUnit, обозначающей метод как тестовый

public class TravellingSalesmanProblemTest { // создание класса

    @Test // Аннотация JUnit, указывающая, что следующий метод является тестовым
    public void tlsTest() { // объявление метода
        TravellingSalesmanProblem tlsClazz = new TravellingSalesmanProblem(); // Создание класса , в котором находится нужный алгоритм

        tlsClazz.taskSolution(); // запуск алгоритма нахождения
    }
}
