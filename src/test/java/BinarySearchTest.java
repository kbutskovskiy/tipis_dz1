import java.util.ArrayList; // Импорт класса ArrayList для создания и работы со списками
import java.util.List; // Импорт интерфейса List для использования списков
import org.example.first_task.BinarySearch; // Импорт класса BinarySearch, который мы тестируем
import org.junit.jupiter.api.Test; // Импорт аннотации @Test для JUnit, обозначающей метод как тестовый

public class BinarySearchTest { // Объявление класса для тестирования бинарного поиска

    @Test // Аннотация JUnit, указывающая, что следующий метод является тестовым
    public void binarySearchTest() { // Метод для тестирования бинарного поиска

        List<Integer> testList = new ArrayList<>(); // Создание списка для тестов
        for (int i = 0; i < 10_000_000; ++i) { // Заполнение списка числами от 0 до 9_999_999
            testList.add(i);
        }

        BinarySearch binarySearch = new BinarySearch(); // Создание объекта бинарного поиска

        // Проведение тестов и проверка результатов
        assert binarySearch.findElement(1, testList) == 1 : "Тест не пройден"; // Элемент 1 должен находиться на позиции 1
        assert binarySearch.findElement(500238, testList) == 500238 : "Тест не пройден"; // Элемент 500238 должен находиться на позиции 500238
        assert binarySearch.findElement(999324, testList) == 999324 : "Тест не пройден"; // Элемент 999324 должен находиться на позиции 999324
        assert binarySearch.findElement(9993249, testList) == 9993249 : "Тест не пройден"; // Элемент 9993249 должен находиться на позиции 9993249
        assert binarySearch.findElement(99932492, testList) == -1 : "Тест не пройден"; // Элемента 99932492 нет в списке, ожидаем -1
        assert binarySearch.findElement(99932492, null) == -1 : "Тест не пройден"; // Тест на передачу null в качестве списка, ожидаем -1
    }
}
